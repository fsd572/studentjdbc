<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.dto.Student" %>
	
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>StudentProfile</title>
</head>
<body>

	<jsp:include page="StuHomePage.jsp" />
	
	<br />
	<center>
		<table border='2'>
			<thead>
				<tr>
					<th>StuId</th>
					<th>StuName</th>
					<th>Gender</th>
					<th>EmailId</th>
					<th>Password</th>
					<th>Course</th>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td> ${ stu.stuId } </td>
			<td> ${ stu.stuName } </td>
			<td> ${ stu.gender } </td>
			<td> ${ stu.emailId } </td>
			<td> ${ stu.password } </td>
			<td> ${ stu.course } </td>
				</tr>
			</tbody>
		</table>
</body>
</html>

