package Assignment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class StuData {

	public static void main(String[] args) {
		Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        System.out.print("Enter Student Name: ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine().trim(); // Read the student name with spaces

        String url = "jdbc:mysql://localhost:3306/fsd57";
        String query = "SELECT * FROM student WHERE name = ?";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, "root", "root");

            pstmt = con.prepareStatement(query);
            pstmt.setString(1, name); // Set the student name in the PreparedStatement

            rs = pstmt.executeQuery();

            if (rs.next()) {
                do {
                	System.out.println("Student Details");
    				System.out.println("----------------");
    				System.out.println("StudentId   : " + rs.getInt(1));
    				System.out.println("Name : " + rs.getString("name"));
    				System.out.println("Gender  : " + rs.getString("gender"));
    				System.out.println("email : " + rs.getString(4));
    				System.out.println("Password: " + rs.getString(5));
    				System.out.println("course : " + rs.getString(6));
    				System.out.println("DOJ : " + rs.getString(7) + "\n");

                } while (rs.next());
            } else {
                System.out.println("Student Record(s) Not Found!!!");
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}


