package Assignment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

//Get the Student Record based on empId using PREPARED STATEMENT;

public class StuId {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter StudentID: ");
		int studentID = scan.nextInt();
		System.out.println();
		
		
		try {		
			pst = con.prepareStatement("Select * from Student where studentID = ?");
			pst.setInt(1, studentID);
			rs = pst.executeQuery();			
			
			if (rs.next()) {				
				System.out.println("Student Details");
				System.out.println("----------------");
				System.out.println("StudentId   : " + rs.getInt(1));
				System.out.println("Name : " + rs.getString("name"));
				System.out.println("Gender  : " + rs.getString("gender"));
				System.out.println("email : " + rs.getString(4));
				System.out.println("Password: " + rs.getString(5));
				System.out.println("course : " + rs.getString(6));
				System.out.println("DOJ : " + rs.getString(7) + "\n");

			} else {
				System.out.println("Student Record Not Found!!!");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
}


